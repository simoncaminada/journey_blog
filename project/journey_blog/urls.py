from django.conf.urls import url
from journey_blog.views import JourneyListView, JourneyCreateView, JourneyUpdateView, JourneyDetailView

urlpatterns = [
    url(r'^$', JourneyListView.as_view(), name='journey_list'),
    url(r'^login/$', "django.contrib.auth.views.login", name='journey_login'),
    url(r'^create/$', JourneyCreateView.as_view(), name='journey_create'),
    url(r'^update/(?P<pk>[0-9]+)/$', JourneyUpdateView.as_view(), name='journey_update'),
    url(r'^update/(?P<pk>[0-9]+)/(?P<destination_pk>[0-9]+)/$', JourneyUpdateView.as_view(), name='journey_update_destination'),
    url(r'^detail/(?P<pk>[0-9]+)/$', JourneyDetailView.as_view(), name='journey_detail'),
    url(r'^detail/(?P<pk>[0-9]+)/(?P<destination_pk>[0-9]+)/$', JourneyDetailView.as_view(), name='journey_detail_destination'),
]
