from ckeditor.fields import RichTextField
from django.core.urlresolvers import reverse_lazy
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


TYPE_TRAIN = "train"
TYPE_BUS = "bus"
TYPE_BOAT = "boat"

TRAVEL_TYPE_CHOICES = (
    (TYPE_TRAIN, _("Train")),
    (TYPE_BUS, _("Bus")),
    (TYPE_BOAT, _("Boat")),
)

# UNPUBLISHED
# PUBLISHED
# DRAFT

class JourneyTravelType(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("Name"))

    def __unicode__(self):
        return self.name

    class Meta():
        verbose_name = _("Journey Travel Type")
        verbose_name_plural = _("Journey Travel Types")
        ordering = ["pk"]


class JourneyDestination(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("Destination"))
    content = RichTextField(verbose_name=_("Content"), blank=True, null=True, config_name="destination_editor")
    draft = RichTextField(verbose_name=_("Draft"), blank=True, null=True)
    types = models.ManyToManyField(JourneyTravelType, verbose_name=_("Typ"))

    def __unicode__(self):
        return self.name

    class Meta():
        verbose_name = _("Journey Destination")
        verbose_name_plural = _("Journey Destinations")
        ordering = ["id"]


class Journey(models.Model):
    title = models.CharField(max_length=255, verbose_name=_("Title"))
    destinations = models.ManyToManyField(JourneyDestination, verbose_name=_("Destinations"), blank=True, null=True)
    author = models.ForeignKey(User, verbose_name=_("Author"), null=True, blank=True)
    description = models.TextField(verbose_name=_("Description"))
    published = models.BooleanField(verbose_name=_("Published"), default=False)
    cdate = models.DateTimeField(auto_now_add=True)
    mdate = models.DateTimeField(auto_now=True)

    def get_absolute_url(self, destination=False):
        if not destination:
            new_destination = self.destinations.last()
        else:
            new_destination = self.destinations.get(pk=destination)
        if new_destination:
            return reverse_lazy("journey_update_destination", kwargs={
                'pk': self.pk,
                'destination_pk': new_destination.pk,
            })
        else:
            return reverse_lazy("journey_update", kwargs={
                'pk': self.pk,
            })

    def __unicode__(self):
        return self.title

    class Meta():
        verbose_name = _("Journey")
        verbose_name_plural = _("Journeys")
        ordering = ["-cdate"]


