from django import forms
from django.forms.models import ModelForm
from journey_blog.models import JourneyDestination, JourneyTravelType, Journey


class JourneyForm(ModelForm):

    class Meta:
        model = Journey


class JourneyAddDestinationForm(ModelForm):
    types = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=JourneyTravelType.objects.all())

    class Meta:
        model = JourneyDestination