from django.contrib.auth.models import User
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from journey_blog.forms import JourneyAddDestinationForm, JourneyForm
from journey_blog.models import Journey, TYPE_TRAIN, TYPE_BUS, TYPE_BOAT, JourneyDestination, JourneyTravelType


class JourneyListView(ListView):
    model = Journey
    template_name = "journey_blog/journey/list.html"
    context_object_name = "journeys"


class JourneyCreateView(CreateView):
    model = Journey
    template_name = "journey_blog/journey/create.html"
    form_class = JourneyForm

    def form_valid(self, form):
        journey = form.save(commit=False)
        journey.author = User.objects.get(pk=1)
        journey.save()
        return super(JourneyCreateView, self).form_valid(form)


class JourneyUpdateView(UpdateView):
    model = Journey
    template_name = "journey_blog/journey/update.html"
    form_class = JourneyAddDestinationForm
    context_object_name = "journey"

    def form_valid(self, form):
        content = self.request.POST.get("content", False)
        published = self.request.POST.get("published", False)
        journey = Journey.objects.get(pk=self.kwargs["pk"])
        if published:
            journey.published = True
        if content != False:
            if self.kwargs.get("destination_pk", False):
                destination = JourneyDestination.objects.get(pk=self.kwargs["destination_pk"])
            else:
                destination = Journey.destinations.first()
            destination.content = content
            destination.save()
            journey.save()
        else:
            name = self.request.POST.get("name", None)
            types = self.request.POST.getlist('types')
            destination = JourneyDestination.objects.create(name=name)
            destination.types.add(*types)
            journey.destinations.add(destination)
            journey.save()
        if published:
            return redirect(reverse_lazy("journey_list"))
        return super(JourneyUpdateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(JourneyUpdateView, self).get_context_data(**kwargs)
        destination_pk = self.kwargs.get("destination_pk", False)
        if destination_pk:
            selected_destination = JourneyDestination.objects.get(pk=destination_pk)
        elif self.object.destinations.count() > 0:
            selected_destination = self.object.destinations.first()
        else:
            selected_destination = False
        if selected_destination:
            context.update({
                "selected_destination": selected_destination,
                "content_form": self.form_class(instance=selected_destination)
            })
        else:
            context.update({
                "selected_destination": selected_destination,
            })
        return context

    def get_success_url(self):
        if self.success_url:
            url = self.success_url % self.object.__dict__
        else:
            try:
                url = self.object.get_absolute_url(self.kwargs.get("destination_pk", False))
            except AttributeError:
                raise ImproperlyConfigured(
                    "No URL to redirect to.  Either provide a url or define"
                    " a get_absolute_url method on the Model.")
        return url


class JourneyDetailView(DetailView):
    model = Journey
    template_name = "journey_blog/journey/detail.html"
    context_object_name = "journey"

    def get_context_data(self, **kwargs):
        context = super(JourneyDetailView, self).get_context_data(**kwargs)
        destination_pk = self.kwargs.get("destination_pk", False)
        if destination_pk:
            selected_destination = JourneyDestination.objects.get(pk=destination_pk)
        elif self.object.destinations.count() > 0:
            selected_destination = self.object.destinations.first()
        else:
            selected_destination = False
        if selected_destination:
            context.update({
                "selected_destination": selected_destination,
            })
        else:
            context.update({
                "selected_destination": selected_destination,
            })
        return context