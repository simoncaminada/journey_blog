$(function() {
    var $content_editor = $("#content_editor");
    var $published_checkbox = $("input#id_published");

    $("[data-action='save']").on("click", function() {
        $content_editor.submit();
    });

    $("[data-action='publish']").on("click", function() {
        $published_checkbox.trigger("click");
        $content_editor.submit();
    });

});