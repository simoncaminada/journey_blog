from django.contrib import admin

from journey_blog.models import Journey, JourneyDestination, JourneyTravelType

admin.site.register([Journey, JourneyDestination, JourneyTravelType])