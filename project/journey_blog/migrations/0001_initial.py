# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Journey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description')),
                ('published', models.BooleanField(default=False, verbose_name='Published')),
                ('cdate', models.DateTimeField(auto_now_add=True)),
                ('mdate', models.DateTimeField(auto_now=True)),
                ('author', models.ForeignKey(verbose_name='Author', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-cdate'],
                'verbose_name': 'Journey',
                'verbose_name_plural': 'Journeys',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JourneyDestination',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Destination')),
                ('content', ckeditor.fields.RichTextField(null=True, verbose_name='Content', blank=True)),
                ('draft', ckeditor.fields.RichTextField(null=True, verbose_name='Draft', blank=True)),
            ],
            options={
                'ordering': ['id'],
                'verbose_name': 'Journey Destination',
                'verbose_name_plural': 'Journey Destinations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JourneyTravelType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Journey Travel Type',
                'verbose_name_plural': 'Journey Travel Types',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='journeydestination',
            name='types',
            field=models.ManyToManyField(to='journey_blog.JourneyTravelType', verbose_name='Typ'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='journey',
            name='destinations',
            field=models.ManyToManyField(to='journey_blog.JourneyDestination', null=True, verbose_name='Destinations', blank=True),
            preserve_default=True,
        ),
    ]
