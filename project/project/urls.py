from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import TemplateView


urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='project/about.html'), name='about'),
    url(r'^tips-tricks/', TemplateView.as_view(template_name='project/tips_tricks.html'), name='tips_tricks'),
    url(r'^experience/', TemplateView.as_view(template_name='project/experience.html'), name='experience'),
    url(r'^journeys/', include('journey_blog.urls')),
    # Admin
    url(r'^admin/', include(admin.site.urls)),
    (r'^ckeditor/', include('ckeditor.urls')),
)
